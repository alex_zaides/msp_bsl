ROOT_PATH=$(realpath ..)# Рутовая папка для всех проектов

#Настройки компилятора
CC_PATH=$(ROOT_PATH)/environment/arm-unknown-linux-uclibcgnueabi/bin
CXX_IC ?=$(CC_PATH)/arm-unknown-linux-uclibcgnueabi-g++
STRIP_IC ?=$(CC_PATH)/arm-unknown-linux-uclibcgnueabi-strip

#Инклюды и либы
INC_PATH = -I./include
#LIB_PATH = -L$(ROOT_PATH)/oasis_baseadapter/bin_ -L$(ROOT_PATH)/oasis_dbgateway/bin_

#Выходной результат
OUT = ./obj_
OUT_BIN = ./bin_
EXECUTABLE = $(OUT_BIN)/msp4305xxflasher



#Флаги компилятора

#Release
CXX_FLAGS = -O3 -g3 -Wall $(INC_PATH)
#LD_FLAGS = $(LIB_PATH) -lsrecord

#Файлы исходников
CC_FILES := $(wildcard src/*.cpp)
OBJ_FILES= $(patsubst src/%.cpp,$(OUT)/%.o,$(CC_FILES))

#Targets ----------------------------------------------------------------------

all : dirs $(EXECUTABLE) Makefile
#	$(MAKE) Makefile.dep
	
#-------------------------------------
$(EXECUTABLE): $(OBJ_FILES) 
	@echo Linking $@ 
	@$(CXX_IC) $(LD_FLAGS) -o $@ $^
	cp $@ $@.unstripped
	@$(STRIP_IC) $@

#-------------------------------------
$(OUT)/%.o: src/%.cpp
	@echo Compiling $^
	@$(CXX_IC) $(CXX_FLAGS) -c -fPIC -o $@ $^

#-------------------------------------
#Makefile.dep:
#	$(CXX) -MM $(CXXFLAGS) *.cpp > $@
#-------------------------------------
.PHONY: dirs
dirs:
	@echo Making dirs
	mkdir -p $(OUT)
	mkdir -p $(OUT_BIN)
clean::
	@echo Cleaning dirs
	rm -rf $(OUT)/*
	rm -rf $(OUT_BIN)/*
	
#-include Makefile.dep
