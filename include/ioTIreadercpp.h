/*
 * ioTIreadercpp.h
 *
 *  Created on: 17 февр. 2015 г.
 *      Author: dobrazcov
 */

#ifndef IOTIREADERCPP_H_
#define IOTIREADERCPP_H_

#include <string>
#include <fstream>
#include <istream>

using namespace std;

class IoTIreader {
public:
	IoTIreader(string file, unsigned short bytes_to_read);
	bool MoreDataToRead();
	unsigned short ReadTI_TextFile(unsigned char* buf);
	~IoTIreader();

private:
	const unsigned short max_bytes_to_read_;
	ifstream ifs_; //поток файла
	int addr_to_write_ ;
	unsigned char passwd_[32];
	bool eof_;
};

#endif /* IOTIREADERCPP_H_ */
