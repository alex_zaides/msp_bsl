/*
 * Logger.h
 *
 *  Created on: 12 февр. 2015 г.
 *      Author: vzhedenov
 */

#ifndef LOGGER_H_
#define LOGGER_H_

#include <string>
#include<iostream>

using namespace std;

class Logger {
public:
	Logger();
	~Logger();
	void SetMode(bool verbose);
	int PrintMsg(const string& msg);
	int PrintHex(const unsigned char* msg, const unsigned short size);
	static string ItoS(int input);
	static string ItoH(int input);
private:
	bool verbose_mode_;
};

#endif /* LOGGER_H_ */
