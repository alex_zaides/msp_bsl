/*
 * port_locker.h
 *
 *  Created on: 24 янв. 2014 г.
 *      Author: dobrazcov
 */

#ifndef PORT_LOCKER_H_
#define PORT_LOCKER_H_

#include <string>
#include "logger.h"

/**
 * Блокировщик порта
 */
class PortLocker
{
public:
	/**
	 * Конструктор
	 */
	PortLocker(Logger* log);
	/**
	 * Блокировка порта
	 * 0 - успешно
	 * -1 - не удалась
	 */
	int LockPort(const std::string& port,int timeout = 0);
	/**
	 * Освобождение порта
	 */
	void UnlockPort(const std::string& port);

private:
	/**
	 * Директория с файлами блокировок
	 */
	static const std::string kLockDir;
	/**
	 * Ключ семафора
	 */
	static const int kSemKey = 1324;
	/**
	 * Максимально допустимое время жизни блокировки порта (в сек)
	 */
	static const int kLockLeaveTimeOut = 600;
	/**
	 * Шаг ожидания (в сек)
	 */
	static const int kWaitStep = 2;
	/**
	 * Объект для работы с логом
	 */
	Logger* const log_;
	/**
	 * Идентификатор семафора
	 */
	int sem_id_;
	/**
	 * Войти в семафор
	 * 0 - успешно
	 * -1 - не удалась
	 */
	int EnterSem(int timeout);
	/**
	 * Покинуть семафорs
	 */
	void LeaveSem();
	/**
	 * Проверка файла блокировки
	 * 1 - блокировка отсутсвует
	 * 0 - блокировка присутсвует
	 * -1 - ошибка
	 */
	int CheckLockFile(const string& loc_file, int pid);
};

#endif /* PORT_LOCKER_H_ */
