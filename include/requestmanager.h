/*
 * Requestmanager.h
 *
 *  Created on: 12 февр. 2015 г.
 *      Author: vzhedenov
 */

#ifndef REQUESTMANAGER_H_
#define REQUESTMANAGER_H_

#include <stddef.h>		//for NULL
#include  <string>
#include "port_locker.h"
#include "serial.h"


class Request_manager {
public:
	/*
	 * Constructor
	 */
	Request_manager(const string port, Logger* logger);
	/*
	 * Open connection
	 */
	int OpenConnection();
	/*
	 * Close connection
	 */
	int CloseConnection();
	/*
	 * Send Password
	 *
	 * pass - pointer to a 32-bytes massive, if not defined default password will be sent
	 */
	int SendPass(const unsigned char* pass = NULL);
	/*
	 * Mass Erase
	 */
	int MassErase();
	/*
	 * Get BSL version
	 */
	string GetVersion();
	/*
	 * Flash TI_txt file into controller
	 *
	 * file - input file
	 */
	int FlashMSP(string file);


	~Request_manager();
private:
	const unsigned short static kBSLBufferSize = 260;
	Logger* const logger_;
	PortLocker port_locker_;
	ComPortManager serial_;
	string port_;
};

#endif /* REQUESTMANAGER_H_ */
