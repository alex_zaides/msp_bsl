/*
 * serial.h
 *
 *  Created on: 13 февр. 2015 г.
 *      Author: dobrazcov
 */

#ifndef SERIAL_H_
#define SERIAL_H_

#include <fcntl.h>
#include <termios.h>
#include <string>
#include "logger.h"

using namespace std;

/**
 * Класс для работы с com-портом
 */
class ComPortManager
{
public:
	/**
	 * Конструктор
	 */
	ComPortManager(string const & path_com, Logger* logger);
	/**
	 * Деструктор
	 */
	virtual ~ComPortManager();
	/**
	 * Отправка команды
	 */
	size_t SendCommands(unsigned char* request, const size_t& req_size, unsigned char* answer, const size_t& resp_size);
	/**
	 * Последняя ошибка от теплосчетчика
	 */
	int LastError();
	/*
	 * InvokeBSL
	 */
	int InvokeBSL();
	/*
	 * Reset to normal mode
	 */
	int Reset();
private:
	/**
	 * Размер читаемых за раз данных
	 */
	const static size_t kReadSize = 50;
	/**
	 * Число попыток операции записи/считывания
	 */
	const static int kRepeatCount = 3;
	/**
	 * Скорость
	 */
	int speed_;
	/**
	 * Путь к com-порту
	 */
	const string path_com_;
	/**
	 * Логгер
	 */
	Logger* logger_;
	/**
	 * Дескриптор файла
	 */
	int fd1_;
	/**
	 * Макс дескриптор файла
	 */
	int maxfd_;
	/**
	 * Настройки порта
	 */
	termios oldtio_, newtio_;
	/**
	 * Набор выходных портов
	 */
	fd_set readfs_;
	/**
	 * Таймаут на ожидание данных
	 */
	timeval timeout_;
	/**
	 * Время последнего запроса
	 */
	struct timespec last_send_;
	/**
	 * Открытие порта
	 */
	bool OpenComPort();
	/**
	 * Закрытие порта
	 */
	void CloseComPort();
	/**
	 * Ожидание данных в порту
	 */
	int WaitPortOutput();
	/**
	 * Добавление CRC
	 */
	void AddCRC(unsigned char* source, const size_t& size);
	/**
	 * Проверка CRC
	 */
	bool CheckCRC(const unsigned char* source, const size_t& size);
	/**
	 * Расчет CRC
	 */
	unsigned short CRCode(const unsigned char *source, const int& size);
	const static unsigned char kCRCOffset=3;
	/*
	 * Управление ножками MSP
	 */
	inline int SetTESTpin(int level);
	inline int SetRSTpin(int level);
	inline void Delay(int msec);
	/*
	 * Выставление ножки
	 */
	int GpioSet(string pin,int value);
};

#endif /* SERIAL_H_ */
