/*
 * ioTIreadercpp.cpp
 *
 *  Created on: 17 февр. 2015 г.
 *      Author: dobrazcov
 */

#include "ioTIreadercpp.h"
#include "logger.h"
#include <cstring>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <cstdio>

IoTIreader::IoTIreader(string file, unsigned short bytes_to_read):max_bytes_to_read_(bytes_to_read)
{
	addr_to_write_ = -1;
	ifs_.open(file.c_str(),ios_base::in);
	for(int i=0;i<32;i++)
		passwd_[i]=0xFF;
	eof_=false;
	//Открытие файла
}

IoTIreader::~IoTIreader() {
	ifs_.close();
}

bool IoTIreader::MoreDataToRead() {

	bool ret =ifs_.eof() || eof_;
	return (!ret);
}

unsigned short IoTIreader::ReadTI_TextFile(unsigned char * buf) {
	unsigned short bytes_read = 0;

	string str;

	if(!ifs_.is_open())
	{
		cout<<"Can't open .txt file"<<endl;
		return -1;
	}

	if( addr_to_write_ == -1)
	{
		getline(ifs_,str);
		sscanf(str.c_str() + 1, "%x\n", &addr_to_write_);
	}
	//Заполняем в посылке адрес
	buf[0] = (unsigned char)(addr_to_write_ &0xFF);
	buf[1] = (unsigned char)(addr_to_write_>>8 &0xFF);
	buf[2] = (unsigned char)(addr_to_write_>>16 &0xFF);

	while((!ifs_.eof()) && (bytes_read < max_bytes_to_read_))
	{
		unsigned int a = 0;
		ifs_ >> str;

		if( str.c_str()[0] == '@' )
		{
		  sscanf(str.c_str() + 1, "%x\n", &addr_to_write_);
		  break;
		}
		else if ( str.c_str()[0] == 'q' || str.c_str()[0] == 'Q' )
		{
			eof_=true;
			addr_to_write_ = -1;
			ofstream pwfs("msp430passwd.txt",ios_base::trunc);
			for (int i=0;i<32;i++)
				pwfs<<Logger::ItoH(passwd_[i])<<" ";
			pwfs.close();
			break;
		}
		else
		{
			sscanf( str.c_str(), "%2x", &a);
			buf[3+bytes_read]=a;
			bytes_read++;
			if(addr_to_write_>=0xFFE0 && addr_to_write_<=0xFFFF)
				passwd_[(addr_to_write_-0xFFE0)]=a;
			addr_to_write_++;
		}
	}
	return bytes_read;
}
