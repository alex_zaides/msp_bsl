/*
 * Logger.cpp
 *
 *  Created on: 12 февр. 2015 г.
 *      Author: vzhedenov
 */

#include "logger.h"
#include <sstream>

Logger::Logger() {
	verbose_mode_ = false;
}

Logger::~Logger() {
}

void Logger::SetMode(bool verbose) {
	verbose_mode_=verbose;
}

int Logger::PrintMsg(const string& msg) {
	int ret=0;
	if(verbose_mode_)
	{
		cout<<msg;
	}
	return ret;
}

int Logger::PrintHex(const unsigned char* msg, const unsigned short size) {
	int ret = 0;
	if(verbose_mode_)
	{
		for (int i = 0; i < size; i++) {
		  cout << hex << (int)msg[i]<<" ";
		}
		cout << endl<< dec;
	}
	return ret;
}

string Logger::ItoS(int input) {
	stringstream ss;
	ss << input;
	string str = ss.str();
	return str;
}

string Logger::ItoH(int input) {
	stringstream ss;
	ss << hex<< input;
	string str = ss.str();
	return str;
}
