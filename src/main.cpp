/*
 * main.cpp
 *
 *  Created on: 12 февр. 2015 г.
 *      Author: vzhedenov
 */
#include "requestmanager.h"
#include <fstream>

int main(int argc, char** argv)
{
	string version="";
	unsigned char pass_buf[32];
	unsigned char* buf_ptr = NULL;
	int result=0;

	if( argc == 1 )
	{
		cout<< "Usage: msp4305xxflasher <filename.txt>\n\n";
		return 1;
	}
	if(argc > 2) // передан файл пароля
	{
		ifstream pw(argv[2],ios_base::in);
		if(pw.is_open())
		{
			unsigned int a;
			for(int i=0;i<32;i++)
			{
				string str;
				pw >>hex>> str;
				sscanf(str.c_str(), "%2x", &a);
				pass_buf[i]=a & 0xFF;
			}
			buf_ptr=pass_buf;
		}
		pw.close();
	}


	Logger logger=Logger();
	logger.SetMode(false);
	Request_manager rm = Request_manager("/dev/ttyS4",&logger);


	if (rm.OpenConnection())
		return -1;
	result = rm.SendPass(buf_ptr);
	if (result  == 5)
	{
		if (rm.SendPass())
			return -1;
	}
	else if (result ==0)
	{
		if (rm.MassErase())
				return -1;
	}
	else
		return -1;

	version=rm.GetVersion();
	cout<<version;

	rm.FlashMSP(argv[1]);
	rm.CloseConnection();

	return 0;
}



