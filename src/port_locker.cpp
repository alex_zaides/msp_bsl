/*
 * port_locker.cpp
 *
 *  Created on: 24 янв. 2014 г.
 *      Author: dobrazcov
 */
#include <sys/sem.h>

#include <fcntl.h>
#include <cstdlib>
#include <errno.h>
#include <signal.h>
#include <libgen.h>
#include <cstring>

#include "port_locker.h"


const string PortLocker::kLockDir = "/var/lock/LCK..";

PortLocker::PortLocker(Logger* log):log_(log),sem_id_(-1)
{}

int PortLocker::LockPort(const string& port,int timeout)
{
	//Код результата
	int ret_code = -1;
	//Формируем путь к loc файлу
	string lock_file = kLockDir + basename((char*)port.c_str());

	//Временный файл для создания блокировки
	char* temp_name = tempnam(NULL, "lck_");

	//PID процесса
	pid_t pid = getpid();

	//Преобразуем PID в HDB UUCP формат
	char pid_str[11];
	ssize_t pid_size = sizeof(pid_str);

	sprintf(pid_str,"% 10d\n",pid);

	//Записываем во временный файл
	int fd = open(temp_name,O_CREAT|O_WRONLY,0644);
	if(fd < 0)
	{
		string msg = "Create temp lock file err:" + string(strerror(errno));
		log_->PrintMsg(msg);

		unlink(temp_name);
		return ret_code;
	}
	if(write(fd,pid_str,pid_size) != pid_size)
	{
		string msg = "Write temp lock file err:" + string(strerror(errno));
		log_->PrintMsg(msg);

		close(fd);
		unlink(temp_name);
		return ret_code;
	}
	close(fd);

	time_t now = time(NULL);

	if(EnterSem(timeout) == 0) //Вход в семафор
	{
		now = time(NULL) - now;
		timeout = timeout - now;
		//Счетчик повторов
		int repeat = timeout > kWaitStep ? timeout/kWaitStep:1;

		if(CheckLockFile(lock_file,pid) >= 0)
		{
			while(repeat > 0)
			{
				//Делаем hard link
				//если удалось, мы получили блокировку
				ret_code = link(temp_name,lock_file.c_str());
				if(ret_code == 0) //0 - успех
					break;
				repeat--;
				sleep(kWaitStep);
			}
		}
		LeaveSem(); //Покидаем семафор
	}

	unlink(temp_name);

	return ret_code;
}

void PortLocker::UnlockPort(const string& port)
{
	//Формируем путь к loc файлу
	string lock_file = kLockDir + basename((char*)port.c_str());
	unlink(lock_file.c_str());
}

int PortLocker::EnterSem(int timeout)
{
	if(sem_id_ < 0)
	{
		//Семафор еще не открыт
		sem_id_ = semget(kSemKey,1,0666);
		if(sem_id_ < 0)
		{
			if(errno == ENOENT)
			{
				//Семафор еще не создан
				sem_id_ = semget(kSemKey,1,IPC_CREAT|0666);
				if(sem_id_ < 0)
				{
					string msg = "Create sem err:" + string(strerror(errno));
					log_->PrintMsg(msg);
					return -1;
				}

				//Настройка
				union semun {
					int val;
					struct semid_ds *buf;
					ushort * array;
				} argument;

				argument.val = 1;

				if( semctl(sem_id_, 0, SETVAL, argument) < 0)
				{
					string msg = "Init sem err:" + string(strerror(errno));
					log_->PrintMsg(msg);
					return -1;
				}
			}
			else
			{
				string msg = "Open sem err:" + string(strerror(errno));
				log_->PrintMsg(msg);
				return -1;
			}
		}
	}

	struct sembuf semops[0];
	semops[0].sem_flg = SEM_UNDO; //Отмена операции при не корректном выходе
	semops[0].sem_num = 0; //Номер семафора в наборе
	semops[0].sem_op = -1; //Операция над семафором

	//Таймаут ожидания семафора
	struct timespec ts;
	ts.tv_sec = (timeout > 0) ? timeout:1;
	ts.tv_nsec = 0;

	if(semtimedop(sem_id_,semops,1,&ts) < 0)
	{
		if(errno != EAGAIN)
		{
			string msg = "Enter sem err:" + string(strerror(errno));
			log_->PrintMsg(msg);
		}
		return -1;
	}
	return 0;
}

void PortLocker::LeaveSem()
{
	if(sem_id_ < 0)
		return;

	struct sembuf semops[0];
	semops[0].sem_flg = IPC_NOWAIT |SEM_UNDO; //Отмена операции при не корректном выходе
	semops[0].sem_num = 0; //Номер семафора в наборе
	semops[0].sem_op = 1; //Операция над семафором

	if(semop(sem_id_,semops,1) < 0)
	{
		string msg = "Leave sem err:" + string(strerror(errno));
		log_->PrintMsg(msg);
	}
}

int PortLocker::CheckLockFile(const string& lock_file, int pid)
{
	//Получаем информацию о файле
	struct stat file_info;

	if(stat(lock_file.c_str(),&file_info) < 0)
	{
		if(errno == ENOENT) //Файл не существует
			return 1;
		else
		{
			string msg = "Read stat err:" + string(strerror(errno));
			log_->PrintMsg(msg);
			return -1;
		}
	}

	//Читаем pid из файла
	int fd = open(lock_file.c_str(),O_RDONLY|O_NDELAY);
	if(fd < 0)
	{
		if(errno == ENOENT)
			return 1; //Файл не существует
		else
		{
			string msg = "Open lock file err:" + string(strerror(errno));
			log_->PrintMsg(msg);
			return -1;
		}
	}
	//Считываем PID
	char pid_str[11];
	ssize_t pid_size = sizeof(pid_str);
	bzero(pid_str,pid_size);

	read(fd,pid_str,pid_size-1);
	close(fd);

	pid_t old_pid = atoi(pid_str);

	if(pid == old_pid)
		return -1;
	else
	{
		if(pid <= 0)
		{
			unlink(lock_file.c_str());
			return 1;
		}

		if(kill(old_pid,0) < 0) //Тестовый сигнал на проверку наличия процесса
		{
			if(errno == ESRCH) //Процесс не существует
			{
				unlink(lock_file.c_str());
				return 1;
			}
			else
			{
				string msg = "Check proc err:" + string(strerror(errno));
				log_->PrintMsg(msg);
				return -1;
			}
		}

		//Проверка времени блокировки
		if(difftime(time(NULL),file_info.st_mtime) > kLockLeaveTimeOut)
		{
			//Считаем что процесс повис
			string msg = "Lock timeout kill";
			log_->PrintMsg(msg);
			kill(old_pid,SIGKILL); //Убиваем процесс
			unlink(lock_file.c_str());
			return 1;
		}
	}

	return 0;
}
