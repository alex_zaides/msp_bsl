/*
 * Requestmanager.cpp
 *
 *  Created on: 12 февр. 2015 г.
 *      Author: dobrazcov
 */

#include "requestmanager.h"
#include "ioTIreadercpp.h"
#include <cstring>
#include <string>
#include "serial.h"


Request_manager::Request_manager(const string port,Logger* logger):logger_(logger),port_locker_(PortLocker(logger)),serial_(ComPortManager(port,logger_)),port_(port)
{
}

int Request_manager::OpenConnection() {

	//Lock port
	port_locker_.LockPort(port_);
	//Invoke BSL
	serial_.InvokeBSL();
	return 0;
}

int Request_manager::CloseConnection() {
	serial_.Reset();
	serial_.~ComPortManager();
	port_locker_.UnlockPort(port_);
	return 0;
}

int Request_manager::SendPass(const unsigned char* pass) {
	cout << "Try to set password"<<endl;
	unsigned char tx_buf[38];
	unsigned char rx_buf[7];
	tx_buf[0]=0x80;
	tx_buf[1]=0x21;
	tx_buf[2]=0x00;
	tx_buf[3]=0x11;
	if (pass == NULL)
	{
		for (int i=0;i<32;i++)
			tx_buf[i+4]=0xFF;
	}
	else
	{
		memcpy(tx_buf+4,pass,32);
	}
	if (serial_.SendCommands(tx_buf,38,rx_buf,7) == 7)
	{
		if (!rx_buf[4])	//status byte ==0
		{
			cout << "\tPassword set successfully"<<endl;
			return 0;
		}
		else if (rx_buf[4]==5)
		{
			cout << "\tIncorrect password"<<endl;
			return 5;
		}
	}
	cout<<"\tCan't set password unknown error"<<endl;
	return -1;
}

string Request_manager::GetVersion() {
	cout << "Try to get version"<<endl;
	unsigned char tx_buf[6];
	unsigned char rx_buf[10];
	string result="";
	tx_buf[0]=0x80;
	tx_buf[1]=0x01;
	tx_buf[2]=0x00;
	tx_buf[3]=0x19;
	if (serial_.SendCommands(tx_buf,6,rx_buf,10) == 10)
	{
		result += "\tVendor: ";
		if(!rx_buf[4])
			result+="TI\n";
		else
			result+="unknown\n";
		result+="\tCI: "+ logger_->ItoS(rx_buf[5]) + "\n\tAPI: " + logger_->ItoS(rx_buf[6]) + "\n\tPI :" + logger_->ItoS(rx_buf[6]) + "\n";
	}
	else
		result+="\tCan't get BSL version";
	return result;
}

int Request_manager::MassErase() {
	cout<<"Erasing flash"<<endl;
	unsigned char tx_buf[6];
	unsigned char rx_buf[7];
	tx_buf[0]=0x80;
	tx_buf[1]=0x01;
	tx_buf[2]=0x00;
	tx_buf[3]=0x15;
	if (serial_.SendCommands(tx_buf,6,rx_buf,7) == 7)
	{
		if (!rx_buf[4])	//status byte ==0
		{
			cout << "\tFlash erased successfully"<<endl;
			return 0;
		}
	}
	logger_->PrintMsg("\tCan't erase flash memory");
	return -1;

}

int Request_manager::FlashMSP(string file) {
	cout<<"Flashing MSP"<<endl;
	unsigned char tx_buf[5+kBSLBufferSize];
	unsigned char rx_buf[7];

	unsigned short length;

	tx_buf[0]=0x80;

	tx_buf[1]=0x55;
	tx_buf[2]=0x55;

	tx_buf[3]=0x10;

	IoTIreader reader(file, kBSLBufferSize - 4);

	while( reader.MoreDataToRead() )
	{
		length = reader.ReadTI_TextFile(tx_buf+4) + 4;

		tx_buf[1] = length & 0xFF;
		tx_buf[2] = length>>8 & 0xFF;

		if ( serial_.SendCommands(tx_buf,length + 5, rx_buf,7) != 7 || rx_buf[4] )
		{
			cout<<"\tError while flashing memory"<<endl;
			return -1;
		}
	}
	cout<<"\tMemory successfully flashed\n";
	return 0;
}

Request_manager::~Request_manager() {
	// TODO Auto-generated destructor stub
}

