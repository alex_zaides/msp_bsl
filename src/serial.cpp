
#include <errno.h>
#include "serial.h"
#include <cstring>
#include <sys/ioctl.h>


ComPortManager::ComPortManager(string const & path_com, Logger* logger)
:path_com_(path_com),logger_(logger)
{
	fd1_ = -1;
	maxfd_= -1;
	last_send_.tv_sec = 0;
}

ComPortManager::~ComPortManager()
{
	 CloseComPort();
}

bool ComPortManager::OpenComPort()
{
	if(fd1_ > 0)
	{
		//logger_->PrintMsg("Port already opened!\n");
		return true;
	}

	//Открытие порта
	fd1_ = open(path_com_.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);

	if (fd1_ < 0)
	{
		string msg = "Open port "+path_com_+" error. " + string(strerror(errno));
		logger_->PrintMsg(msg);
		return false;
	}

	//Сохранение старых настроек
	tcgetattr(fd1_, &oldtio_);

	//Инициализация новых настроек
	bzero(&newtio_, sizeof(newtio_));

	//9600 8E1
	newtio_.c_cflag =   CLOCAL | CREAD ;
	newtio_.c_cflag |=  B9600;
	newtio_.c_cflag |=  PARENB;
	newtio_.c_cflag &= ~PARODD;
	newtio_.c_cflag &= ~CSTOPB;
	newtio_.c_iflag |=  IGNPAR;
	newtio_.c_cflag |=  CS8;

	newtio_.c_cc[VTIME]    = 1;   /* inter-character timer used */
	newtio_.c_cc[VMIN]     = 1;   /* blocking read until 1000 chars received */

	tcsetattr(fd1_,TCSANOW,&newtio_);

	logger_->PrintMsg("Port opened:" + path_com_ + "\n");

	return true;
}

void ComPortManager::CloseComPort()
{
	if(fd1_ > 0)
	{
		tcsetattr(fd1_, TCSANOW, &oldtio_);
		close(fd1_);

		fd1_ = -1;
	}
}

size_t ComPortManager::SendCommands(unsigned char* request, const size_t& req_size, unsigned char* answer, const size_t& resp_size)
{
	if(!OpenComPort())
		return -1;

	//Максимальное число попыток чтения
	int repeat_cnt = kRepeatCount;

	size_t read_cnt = 0; //Счетчик считанных данных

	//Размер записанных считанных/данных
	ssize_t isize=0;
	ssize_t osize=0;

	//Добавляем CRC
	AddCRC(request,req_size);

	//Код состояния функций работы с портом
	int code;

	do
	{
		//Очистка от не отправленных/считанных данных
		tcflush(fd1_, TCIOFLUSH);

		//Запись данных в порт
		isize = write(fd1_, request, req_size);
		clock_gettime(CLOCK_MONOTONIC,&last_send_);

		if(isize <= 0 || (size_t)isize < req_size)
		{
			string msg = "Write port error. " + string(strerror(errno));
			logger_->PrintMsg(msg);
			continue; //Не правильно записалось
		}

		logger_->PrintMsg("Write to port:\n");
		logger_->PrintHex((unsigned char*)request,req_size);

		bool need_repeat = true; //Необходимость перезаписи

		//Ждем ACK
		unsigned char ack=0;
		int ack_get_code = WaitPortOutput(); //Ожидаем данные на чтение
		if(ack_get_code > 0)
		{
			if(FD_ISSET(fd1_, &readfs_))
			{
				//Читаем данные
				osize = read(fd1_, &ack, 1);
				if (ack) //ack is faslse
				{
					logger_->PrintMsg("Bad ACK:" );
					logger_->PrintHex(&ack,1);
					continue;
				}
			}
			else
			{
				continue;
			}


			//Подготавливаем буфер под ответ
			bzero(answer,resp_size);

			read_cnt = 0; //Счетчик считанных данных
			int read_err_cnt = 100; //Счетчик попыток считывания (если пойдет бесконечный поток мусора)
			char buffer[kReadSize]; //Буфер

			do
			{
				//Уменьшаем счетчик попыток чтения
				read_err_cnt--;

				if(read_err_cnt < 0) //Так долго читать не можем возможно нужно увеличить таймаут
				{
					//Логирование
					logger_->PrintMsg("Slow read speed. Read cnt limit. Response:");
					logger_->PrintHex((unsigned char*)answer,read_cnt);
					need_repeat = false;
					break;
				}

				osize = 0;
				code = WaitPortOutput(); //Ожидаем данные на чтение
				if(code > 0)
				{
					if(FD_ISSET(fd1_, &readfs_))
					{
						//Читаем данные
						osize = read(fd1_, buffer, kReadSize);

						if(osize <= 0) //Почему-то не прочиталось
						{
							string msg = "Select port error. " + string(strerror(errno));
							logger_->PrintMsg(msg);
							continue;
						}

//						logger_->PrintMsg("Read:" + logger_->ItoS(osize) + "Total:" + logger_->ItoS(read_cnt) + "Expected:" + logger_->ItoS(resp_size) + "\n");
//						logger_->PrintMsg("Read from port:");
//						logger_->PrintHex((unsigned char*)buffer,osize);

						if(read_cnt + osize > resp_size)
						{
							//Не можем больше сохранять весь ответ заполнен
							logger_->PrintMsg("Response too large:");
							logger_->PrintHex((unsigned char*)answer,read_cnt);
							logger_->PrintHex((unsigned char*)buffer,osize);
							osize = resp_size - read_cnt;
						}
						//Копируем в ответ
						memcpy(answer + read_cnt,buffer,osize);

						//Увеличиваем счетчик считанных байт
						read_cnt += osize;
						if(read_cnt == resp_size) //Считали то что хотели
						{
							need_repeat = false;
							break;
						}
					}
				}
				else if(code == 0)
				{
					//Вышли по таймауту
					logger_->PrintMsg("Select port timeout. Next try...\n");
					need_repeat = true;
					break;
				}
				else if(code < 0)
				{
					//Какие-то проблемы в канале
					//Логируем
					string msg = "Select port error. " + string(strerror(errno));
					logger_->PrintMsg(msg);
					need_repeat = true; //Повторяем запись и чтение
					break;
				}

			} while(code > 0);
		}
		else if(ack_get_code== 0)
		{
			//Вышли по таймауту
			logger_->PrintMsg("Select port timeout. Next try...\n");
			need_repeat = true;
		}
		else if(ack_get_code < 0)
		{
			//Какие-то проблемы в канале
			//Логируем
			string msg = "Select port error. " + string(strerror(errno));
			logger_->PrintMsg(msg);
			need_repeat = true; //Повторяем запись и чтение
		}

		if(!need_repeat)
		{
			if(read_cnt == 0)
				break;

			//Проверяем CRC
			if(!CheckCRC(answer,read_cnt))
			{
				logger_->PrintMsg("Invalid CRC\n");
				continue;
			}

//			logger_->PrintMsg("Request:\n");
//			logger_->PrintHex((unsigned char*)request,req_size);
			logger_->PrintMsg("Response:\n");
			logger_->PrintHex((unsigned char*)answer,read_cnt);

			break;
		}
	} while(--repeat_cnt > 0);

	if(repeat_cnt <=0)
	{
		logger_->PrintMsg("Limit of IO operations repeat\n");
		read_cnt = 0;
	}
	return read_cnt;
}

void ComPortManager::AddCRC(unsigned char* source,const size_t& size)
{
	if(size <= 5)
		return;

	unsigned short test = CRCode(source+kCRCOffset,size-2-kCRCOffset);

	memcpy(source+size-2,&test,2);
}

bool ComPortManager::CheckCRC(const unsigned char* source, const size_t& size)
{
	if(size <= 2)
		return false;

	void * test = NULL;
	test = (void *)(source + size -2);

// 	logger_->PrintMsg("CRC is ");
//	logger_->PrintHex((unsigned char *)(test),2);

	unsigned short test_1;

	test_1 = *((unsigned short *)(source + size -2));

	return test_1 == CRCode(source+kCRCOffset,size-2-kCRCOffset);
}

unsigned short ComPortManager::CRCode(const unsigned char* source, const int& size)
{
	unsigned short crc = 0xFFFF;
	for( int i = 0; i < size; i++ )
	{
		unsigned short int x;
	  x = ((crc>>8) ^ source[i]) & 0xff;
	  x ^= x>>4;
	  crc = (crc << 8) ^ (x << 12) ^ (x <<5) ^ x;
	}

	return crc;
}

int ComPortManager::WaitPortOutput()
{
	FD_ZERO(&readfs_);
	FD_SET(fd1_, &readfs_);  /* set testing for source */

	timeout_.tv_usec = 500000;  /* microseconds */
	timeout_.tv_sec  = 1;  /* seconds */

	maxfd_ = fd1_ + 1;
	return select(maxfd_, &readfs_, NULL, NULL, &timeout_);
}

int ComPortManager::LastError()
{
	return errno;
}

inline int ComPortManager::SetTESTpin(int level) {
	return GpioSet(string("147"), level);
}

inline int ComPortManager::SetRSTpin(int level) {
	return GpioSet(string("146"), level);
}

int ComPortManager::InvokeBSL() {
	int ret=0;

	//Configure port
	OpenComPort();

	ret=SetTESTpin(0);
	ret=SetRSTpin(0);

	ret=SetTESTpin(1); /* TEST pin: Vcc */
	Delay(10);
	ret=SetTESTpin(0); /* TEST pin: GND */
	Delay(10);
	ret=SetTESTpin(1); /* TEST pin: Vcc */
	Delay(10);
	ret=SetRSTpin (1); /* RST  pin: Vcc */
	Delay(10);
	ret=SetTESTpin(0); /* TEST pin: GND */

	Delay(350);

	return ret;
}

inline void ComPortManager::Delay(int msec) {
	usleep(msec*1000);
}

int ComPortManager::GpioSet(string pin, int value) {
	/*export*/
	int exportfd, directionfd, valuefd;
	char str[100];
	exportfd = open("/sys/class/gpio/export", O_WRONLY);
	if (exportfd < 0){
		cout << "Cannot open GPIO " << pin << "to export it"<< exportfd <<endl;
		return -1;
	}
	write(exportfd, pin.c_str(), 4);
	close(exportfd);
	/*direction*/
	sprintf(str,"/sys/class/gpio/gpio%s/direction",pin.c_str());
	directionfd = open(str, O_RDWR);
	if (directionfd < 0){
		cout << "Cannot open GPIO direction for " << pin << endl;
		return -1;
	}
	write(directionfd, "out", 4);
	close(directionfd);
	/*val*/
	sprintf(str,"/sys/class/gpio/gpio%s/value",pin.c_str());
	valuefd = open(str, O_RDWR);
	if (valuefd < 0){
		printf("Cannot open GPIO value for %s\n",pin.c_str());
		return -1;
	}
	if(value) write(valuefd, "1", 2);
	else    write(valuefd, "0", 2);
	close(valuefd);
	return 0;
}

int ComPortManager::Reset() {
	logger_->PrintMsg("Resetting BSL\n");
	int ret = 0;
	ret=SetTESTpin(1); /* TEST pin: Vcc */
	Delay(10);
	ret=SetTESTpin(0); /* TEST pin: GND */
	Delay(10);
	ret=SetTESTpin(1); /* TEST pin: Vcc */
	Delay(10);
	ret=SetTESTpin(0); /* TEST pin: GND */
	Delay(10);
	ret=SetRSTpin (0); /* RST  pin: GND */
	Delay(10);
	ret=SetRSTpin (1); /* RST  pin: Vcc */
	return ret;
}
